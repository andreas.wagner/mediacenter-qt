#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QHBoxLayout>
#include <QLabel>
#include <QMediaPlayer>
#include <QMouseEvent>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    buttonContainerWidget = new QWidget();
    buttonContainerWidget->setParent(ui->labelAsScrollArea);
    //buttonContainerWidget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
    buttonContainerWidget->resize(1000, buttonContainerWidget->height());
    //    QHBoxLayout* hboxLayout = new QHBoxLayout(buttonContainerWidget);

    for (int i = 0; i < 29; i++)
    {
        QPushButton* button = new QPushButton(QString::number(i));
        addButtonToView((button));
    }


    mediaPlayer = new QMediaPlayer();

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::addButtonToView(QPushButton* button)
{
    int buttonWidth = 150;
    int buttonSpace = 15;
    button->setFixedSize(buttonWidth,buttonWidth);
    //  QLayout* hboxLayout = buttonContainerWidget->layout();
    //hboxLayout->addWidget(button);
    buttons.append(button);
    button->setParent(buttonContainerWidget);
    button->installEventFilter(this);

    int rightPos = (buttons.count()) * (buttonSpace+buttonWidth);
    int leftPos = rightPos - buttonWidth;

    button->setGeometry(leftPos, buttonSpace, button->width(), button->height());

    buttonContainerWidget->setGeometry(0,0, rightPos, buttonContainerWidget->height());

    //    double buttonsRightEnd = button->pos().x() + button->width();
    //    double widgetEnd = buttonContainerWidget->width();


    //    if (buttonsRightEnd > widgetEnd)
    //    {
    //        buttonContainerWidget->setFixedWidth(buttonsRightEnd);
    //    }
}


bool MainWindow::eventFilter( QObject *o, QEvent *e )
{
    QPushButton* asButton = dynamic_cast<QPushButton*>(o);

    if (asButton == NULL)
        return false; // Es ist das falsche Objekt (Kann aber aktuell nicht passieren)

    // **** Start ****

    // Mouse
    if ( e->type() == QEvent::MouseButtonPress )
    {
        QMouseEvent* mouseEvent = (QMouseEvent*)(e);
        areaMoveStart(mouseEvent->pos().x());
    }

    // Touch
    if ( e->type() == QEvent::TouchBegin)
    {
        QTouchEvent* touchEvent = (QTouchEvent*)(e);
        QEventPoint point = touchEvent->point(1);
        areaMoveStart(point.position().x());
    }

    //  **** Update / Move ****

    // Mouse
    else if ( e->type() == QEvent::MouseMove)
    {
        QMouseEvent* mouseEvent = (QMouseEvent*)(e);
        areaMove(mouseEvent->pos().x());
    }

    // Touch
    else if ( e->type() == QEvent::TouchUpdate)
    {
        QTouchEvent* touchEvent = (QTouchEvent*)(e);
        QEventPoint point = touchEvent->point(1);
        areaMove(point.position().x());
    }

    //  **** End ****

    // Mouse
    else if ( e->type() == QEvent::MouseButtonRelease)
    {
        QMouseEvent* mouseEvent = (QMouseEvent*)(e);
        areaMoveEnd(mouseEvent->pos().x());
    }

    // Touch
    else if ( e->type() == QEvent::TouchEnd ||  e->type() == QEvent::TouchCancel)
    {
        QTouchEvent* touchEvent = (QTouchEvent*)(e);
        QEventPoint point = touchEvent->point(1);
        areaMoveEnd(point.position().x());
    }

    // Other event, return unsed info (false)
    else
    {
        return false;
    }

    debugShowMousePositions();
    return true;
}


bool MainWindow::areaMoveStart(double x)
{
    mousePos = x;
    lastPos = x;
    return true; // Rückgabe noch nicht verwendet
}


bool MainWindow::areaMove(double x)
{
    mousePos = x;
    double delta = x - lastPos; // Maus links: negativ
    scrolledPos += delta;

    // Wenn der erste linke Knopf nach rechts wandern soll, also leerer Bereich nach rechts gezogen wird => stop!
    if (scrolledPos > 0)
    {
        delta -= scrolledPos;
        scrolledPos = 0;
    }

    int max = buttonContainerWidget->width() - 150;

    if (scrolledPos < - max)
    {
        scrolledPos = -max;
    }

    int bx = buttonContainerWidget->pos().x();
    int by = buttonContainerWidget->pos().y();
    int bw = buttonContainerWidget->width();
    int bh = buttonContainerWidget->height();
    buttonContainerWidget->setGeometry(scrolledPos, by, bw, bh);

    //  ui->scrollArea->scroll(delta,0);


    return true; // Rückgabe noch nicht verwendet
}

bool MainWindow::areaMoveEnd(double x)
{
    mousePos = x;
    lastPos = x;
    return true; // Rückgabe noch nicht verwendet
}

void MainWindow::play()
{

}

void MainWindow::debugShowMousePositions()
{
    QString text = "";

    text += "lastpos = " + QString::number(lastPos) + "\n";
    text += "scrolledPos = " + QString::number(scrolledPos) + "\n";
    text += "mousePos = " + QString::number(mousePos) + "\n";

    ui->debugLabel->setText(text);
}
