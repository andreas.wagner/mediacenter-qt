#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QList>
#include <QMainWindow>
#include <QPushButton>

class QMediaPlayer;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    bool eventFilter( QObject *o, QEvent *e );

    void addButtonToView(QPushButton* newButton);

    bool areaMoveStart(double x);
    bool areaMove(double x);
    bool areaMoveEnd(double x);

    public slots:

    void play();

private:
    Ui::MainWindow *ui;

    QMediaPlayer* mediaPlayer;

    QWidget* buttonContainerWidget;

    double lastPos = 0;
    double scrolledPos = 0;

    QList<QPushButton*> buttons;
    //double scrollSpeed;
    double mousePos;
    void debugShowMousePositions();
};
#endif // MAINWINDOW_H
